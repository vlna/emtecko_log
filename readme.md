# Emtecko log 

A simple tool to download and process call logs from Emtéčko - Czech mobile phone service provider

## Usage

```emtecko_log.py [-h] -u USERNAME -p PASSWORD [-f {sms,out,data}]```

## Libraries

* BeautifulSoup4

## Licence

This software is licensed under the GNU General Public License version 3
with the addition of the following special exception:

In addition, as a special exception, the copyright holders give permission to redistribute
all this program or portions of this program without the full text of GNU General Public
License version 3. You must obey the GNU General Public License in all other respects for all
the code. If you modify file(s) with this exception, you may extend this exception to your
version of the file(s), but you are not obligated to do so. If you do not wish to do so,
delete this exception statement from your version. If you delete this exception statement
from all source files in the program, then also delete it here.
