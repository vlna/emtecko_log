# -*- coding: utf-8 -*-

# Author/Copyright: Vladimír Návrat <>
# License: GPLv3 with exception

import logging
import requests
import argparse
from bs4 import BeautifulSoup

class Parameters:
    def __init__(self):
        parser = argparse.ArgumentParser()
        parser.add_argument('-u', '--username', help='Username', required=True)
        parser.add_argument('-p', '--password', help='Password', required=True)
        parser.add_argument('-f', '--filter', help='Filter', choices=('sms', 'out', 'data'))
        args = parser.parse_args()

        self.username = args.username
        self.password = args.password
        self.filter = args.filter


class Emtecko:
    session = requests.Session()
    raw_log = []

    filter_helper = {'sms': 'SMS', 'out': 'Odchozí hovor', 'data': 'Data'}

    def login(self, username, password):
        r = self.session.get('https://samoobsluha.emtecko.cz/Login')
        soup = BeautifulSoup(r.text, 'html.parser')
        token = soup.find('input', attrs={'name': '__RequestVerificationToken'})['value']

        data = {'LoginValue': username, 'PasswordValue': password, '__RequestVerificationToken': token}
        r = self.session.post('https://samoobsluha.emtecko.cz/Login?handler=Login', data=data)
        if 'class="logout"' not in r.text:
            logging.error('Unable to login')
            raise Exception('LoginError')

    def process_page(self, page):
        logging.info('Processing page ...')
        soup = BeautifulSoup(page, 'html.parser')
        table = soup.find('table', class_='table-responsive-md')
        for row in table.find('tbody').find_all('tr'):
            self.raw_log.append(tuple([item.text.strip() for item in row.find_all('td')]))
        return soup.find('li', attrs={'area-label': 'DALŠÍ'}).find('a').get('href')

    def get_log(self):
        logging.info('Extracting log ...')
        r = self.session.get('https://samoobsluha.emtecko.cz/List?&pagerCalls=pagerCallHistory')
        next_page = self.process_page(r.text)
        while next_page:
            r = self.session.get('https://samoobsluha.emtecko.cz/List%s' % next_page)
            next_page = self.process_page(r.text)
        return self.raw_log

    def beauty_log(self):
        logging.info('Beautifying ...')
        return map(lambda _: ('%s%s %s' % tuple(_[0].split()), _[1] if len(_[1]) == 9 else '420%s' % _[1],
                              _[2], _[3].replace('\xa0Kč', ''), *_[4:]), self.raw_log)

    def filter_log(self, limit):
        logging.info('Filtering ...')
        return filter(lambda _: _[4] == self.filter_helper[limit], self.beauty_log())


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO, format='%(asctime)s %(funcName)14s [%(levelname)s]: %(message)s')
    logging.info('Emtéčko Log')
    logging.info('(c) 2021 Vladimír Návrat')

    parameters = Parameters()
    emtecko = Emtecko()

    emtecko.login(parameters.username, parameters.password)
    emtecko.get_log()
    log = emtecko.filter_log(parameters.filter) if parameters.filter else emtecko.beauty_log()

    header = ('Datum čas', 'Číslo', 'Jednotek', 'Cena', 'Typ služby', 'Objem dat')
    with open('call_log.csv', 'w') as f:
        print('"%s";"%s";"%s";"%s";"%s";"%s"' % header, file=f)
        for record in log:
            print('"%s";"%s";"%s";"%s";"%s";"%s"' % record, file=f)
